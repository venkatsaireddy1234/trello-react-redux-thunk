import "./App.css";
import Boards from "./Components/Boards";
import { BrowserRouter as Router, Route, Switch ,Redirect} from "react-router-dom";
import NavBar from "./Components/NavBar";
import React, { Component } from "react";

export class App extends Component {
  render() {
    return (
      <Router>
        <React.Fragment>
          <NavBar />
          <Switch>
            <Route exact path="/">
              <Redirect to="/boards"/>
            </Route>
            <Route exact path="/boards" component={Boards} />
            {/* <Route exact path="/boards/:boardId" component={DetailedEachBoard} /> */}
          </Switch>
        </React.Fragment>
      </Router>
    );
  }
}

export default App;

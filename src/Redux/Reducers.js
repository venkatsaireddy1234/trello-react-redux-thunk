import * as actions from "./Action"
import { combineReducers } from "redux";


 const boardsReducer =(boards = [], action)=>{
    switch (action.type){
        case actions.DISPLAY_BOARDS:
            return [...action.payload]
        default:
            return boards;
    }
}

export default combineReducers({boardsReducer})
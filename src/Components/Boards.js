import React, { Component } from 'react';
import { getBoards } from '../Redux/Action';
import { connect } from "react-redux";
import EachBoard from './EachBoard';

class Boards extends Component {
    state = {  } 

    componentDidMount=()=>{
        this.props.dispatch(getBoards());
    }

    render() { 
        return (
        <div>
        <div className=" yyy">
          {this.props.boards.map((board) => (
            <EachBoard key={board.id} eachBoard={board} />
          ))}
        </div>
        </div>
        
        );
    }
}


const mapStateToProps = (state)=>{
    return{
        boards:state.boardsReducer
    }
}
 
export default connect(mapStateToProps)(Boards);
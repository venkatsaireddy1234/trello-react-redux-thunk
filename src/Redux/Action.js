import { getBoardsData } from "../Apis"


export const DISPLAY_BOARDS = "boards/getboards"

export const getBoards = ()=>{
    return(dispatch)=>{
        return getBoardsData().then((boards)=>{
            dispatch({
                type:DISPLAY_BOARDS,
                payload:boards
            })
        })
    }
}




    